// /* TASK - 6
// * Create a start screen of the game:
// * There must be no pacmen on the screen, only modal window that will be asking the user
// * if he wants to start the game.
// * If user chose to start the game - remove modal window and show:
// *   pacmen itself
// *   the list of game modes, and highlight the current mode with red color and make it text bold.
// *   amount of game-points. By default - 0
// *
// * Add a food. It must be the square, with different background every time, placed randomly
// * and when the pacmen position is matches the food position,
// * food can disappear, you increase the value of game-points by 1
// * and place another food randomly on the page.
// */
//
// // const background = document.querySelector(".background");
// // background.style.height = window.innerHeight + "px";
//
// const script = document.querySelector("script");
// const background = document.createElement("div");
// background.style.height = window.innerHeight + "px";
// background.classList.add("background");
// script.before(background);
//
// let mode = 50;
// const foods = [];
// let eatenFoodCounter = 0;
//
// function startPage() {
//     const container = document.createElement("div");
//     container.style.height = window.innerHeight + "px";
//     container.classList.add("container");
//     const startPageHeader = document.createElement("h3");
//     startPageHeader.classList.add("intro-header");
//     startPageHeader.innerText="Welcome to the Pacman Game";
//     container.append(startPageHeader);
//
//     const startPageButton = document.createElement("button");
//     startPageButton.classList.add("intro-start-button");
//     startPageButton.innerText="Start the Game";
//     container.append(startPageButton);
//
//     background.append(container);
//
//     startPageButton.addEventListener('click',() =>{
//         background.innerHTML="";
//         return startGame();
//     })
// }
//
// function startGame() {
//     const score = new CreateScoreAndMode();
//     score.mode25.addEventListener('click', () => {
//         score.mode25.classList.add("selected-mode");
//         score.mode50.classList.remove("selected-mode");
//         score.mode100.classList.remove("selected-mode");
//         mode = 25;});
//     score.mode50.addEventListener('click', () => {
//         score.mode25.classList.remove("selected-mode");
//         score.mode50.classList.add("selected-mode");
//         score.mode100.classList.remove("selected-mode");
//         mode = 50;});
//     score.mode100.addEventListener('click', () => {
//         score.mode25.classList.remove("selected-mode");
//         score.mode50.classList.remove("selected-mode");
//         score.mode100.classList.add("selected-mode");
//         mode = 100;});
//     const pacman = new CreatePacman();
//     const eatingSoundEffect = new Sound("sound/eatingeffect.mp3");
//     pacmanMoves();
//     addFood();
//     timer(score.timer);
//     pacmanEatsFood(pacman.pacmanContainer, score.scoreAndModeScore, eatingSoundEffect);
// }
//
// function CreateScoreAndMode() {
//     this.scoreAndMode = document.createElement("div");
//     this.scoreAndMode.classList.add("score-and-mode");
//     background.append(this.scoreAndMode);
//
//     this.scoreAndModeModes = document.createElement("scoreAndModeModes");
//     this.scoreAndModeModes.classList.add("score-and-mode-modes");
//     this.scoreAndMode.append(this.scoreAndModeModes);
//
//     this.mode25 = createModeButton("Speed: 25");
//     this.mode50 = createModeButton("Speed: 50");
//     this.mode50.classList.add("selected-mode");
//     this.mode100 = createModeButton("Speed: 100");
//
//
//     this.scoreAndModeModes.append( this.mode25,this.mode50 , this.mode100 );
//
//     this.timer = document.createElement("span");
//     this.timer.classList.add("score-and-mode-score");
//     this.timer.innerText = "Time Left: 15";
//     this.scoreAndModeModes.append(this.timer);
//
//     this.scoreAndModeScore = document.createElement("span");
//     this.scoreAndModeScore.classList.add("score-and-mode-score");
//     this.scoreAndModeScore.innerText = "Score: 0";
//     this.scoreAndMode.append(this.scoreAndModeScore);
//
//
// }
//
// function createModeButton(innerText) {
//     const button = document.createElement("button");
//     button.classList.add("mode");
//     button.innerText=innerText;
//     return button;
// }
//
// function CreatePacman() {
//     this.pacmanContainer = document.createElement("div");
//     this.pacmanContainer.classList.add("pacman");
//     background.append(this.pacmanContainer);
//
//     this.pacmanTop = document.createElement("div");
//     this.pacmanTop.classList.add("pacman-top");
//     this.pacmanContainer.append(this.pacmanTop);
//
//     this.pacmanBottom = document.createElement("div");
//     this.pacmanBottom.classList.add("pacman-bottom");
//     this.pacmanContainer.append(this.pacmanBottom);
// }
//
// function pacmanMoves() {
//     const pacmanContainer = document.querySelector(".pacman");
//
//
//     document.addEventListener('keyup', (event) => {
//             console.log(event.key);
//             if (event.key === "ArrowRight") { //window.inner
//                 let left = isNaN(parseInt(pacmanContainer.style.left)) ? 0 : parseInt(pacmanContainer.style.left);
//                 if( (left + 210 + mode<window.innerWidth) )
//                     pacmanContainer.style.left = left + mode + 'px';
//
//                 pacmanContainer.style.transform = 'rotate(0deg)'
//             }
//             else if (event.key === "ArrowLeft") { //window.inner
//                 let left = isNaN(parseInt(pacmanContainer.style.left)) ? 0 : parseInt(pacmanContainer.style.left);
//                 if(left-mode>=0)
//                     pacmanContainer.style.left = left - mode + 'px';
//
//                 pacmanContainer.style.transform = 'scaleX(-1)'
//             }
//             else if (event.key === "ArrowUp") { //window.inner
//                 let top = isNaN(parseInt(pacmanContainer.style.top)) ? 80 : parseInt(pacmanContainer.style.top);
//                 if(top-mode>=80)
//                     pacmanContainer.style.top = top - mode + 'px';
//
//                 pacmanContainer.style.transform = 'rotate(270deg)'
//             }
//             else if (event.key === "ArrowDown") { //window.inner
//                 let top = isNaN(parseInt(pacmanContainer.style.top)) ? 80 : parseInt(pacmanContainer.style.top);
//                 if(top + 210 + mode < window.innerHeight)
//                     pacmanContainer.style.top = top + mode + 'px';
//
//                 pacmanContainer.style.transform = 'rotate(90deg)'
//             }
//             else if(event.key === "1" && event.ctrlKey){
//                 mode=25;
//             }
//             else if(event.key === "2" && event.ctrlKey){
//                 mode=50;
//             }else if(event.key === "3" && event.ctrlKey){
//                 mode=100;
//             }
//         }
//     );
// }
//
// function CreateFood() {
//     this.food = document.createElement("div");
//     this.food.classList.add("food");
//     this.food.style.left = Math.floor(Math.random() * ( (window.innerWidth-60) -30) ) +"px";
//     this.food.style.top = Math.floor(Math.random() * ((window.innerHeight-60) - 100) ) + 85 +"px";
//     this.food.style.backgroundColor = randomColor();
//     background.append(this.food);
// }
//
// function randomColor() {
//     let red = Math.floor(Math.random() * 254);
//     let green = Math.floor(Math.random() * 254);
//     let blue = Math.floor(Math.random() * 254);
//     return `rgb(${red}, ${green}, ${blue})`
// }
//
// function addFood() {
//     let i=0;
//     let interval = setInterval(() =>{
//         foods[i] = new CreateFood();
//         i++;
//         if(i===20){  //amount of food
//             clearInterval(interval);
//         }
//     }, 750);
//
// }
//
// function timer(timer) {
//     let time = 15;
//     let interval = setInterval(()=>{
//         time--;
//         timer.innerText = `Time Left: ${time}`;
//         if (time===0){
//             clearInterval(interval);
//             alert("game over");
//         }
//     },1000);
//
// }
//
// function pacmanEatsFood(pacman,score,sound) {
//     document.addEventListener('keyup',(event) => {
//         foods.forEach((item,index) => {
//             if( (pacman.offsetLeft<=item.food.offsetLeft && (pacman.offsetLeft+200) >= item.food.offsetLeft) && (pacman.offsetTop<=item.food.offsetTop && (pacman.offsetTop+200) >= item.food.offsetTop)){
//                 foods[index].food.remove();
//                 sound.play();
//                 eatenFoodCounter++;
//                 score.innerText = `Score: ${eatenFoodCounter}`;
//             }
//         });
//     })
// }
//
// function Sound(src) {
//     this.sound = document.createElement("audio");
//     this.sound.src = src;
//     this.sound.setAttribute("preload", "auto");
//     this.sound.setAttribute("controls", "none");
//     this.sound.style.display = "none";
//     document.body.appendChild(this.sound);
//     this.play = function(){
//         this.sound.play();
//     };
//     this.stop = function(){
//         this.sound.pause();
//     }
// }
//
//
// startPage();
